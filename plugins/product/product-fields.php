<?php

//patients
function add_custom_metabox1(){
	add_meta_box(
		'product_meta',
		__('Products'),
		'meta_callback1',
		'product',
		'normal',
		'core'
		);
}
add_action('add_meta_boxes', 'add_custom_metabox1');

function meta_callback1( $post){
	//register the form to database
	wp_nonce_field(basename(__FILE__), 'products_nonce');
	$stored_meta = get_post_meta( $post->ID);
	?>




      <div>
        <div class="meta-row">
            <div class="meta-th">
              <label for="product_name" class="dwwp-row-title"><?php _e('Product Name','product-listing'); ?></label>
            </div>
            <div class-"meta-td">
              <input type="text" name="product_name" id="product-name" value""/>

            </div>
        </div>

                <div class="meta-row">
            <div class="meta-th">
              <label for="product_description" class="dwwp-row-title"><?php _e('Product Description','product-listing'); ?></label>
            </div>
            <div class-"meta-td">
              <input type="text" name="product_description" id="product-description" >

            </div>
        </div>

          <!--   <div class="meta-row">
              <div class="meta-th">
                <label for="alergy" class="dwwp-row-title">Alergy</label>
              </div>
              <div class-"meta-td">
                <input type="text" class="dwwp-row-content" name="patient_alergy" id="patient-alergy"/> 
                                   
              </div>
          </div> -->
      </div>
     
      <div class="meta">
      	 <div class="meta-th">
      		  <span>Principle Duties</span>
         </div>
      </div>
      <div class="meta-editor"></div>

    <?php


        $content = get_post_meta( $post->ID, 'principle_duties', true);
        $editor = 'principle_duties';
        $settings = array(
        	'textarea_rows' =>8,
        	'media_buttons' => true,

        	);
        wp_editor( $content, $editor, $settings);
    ?>
    </div>
<?php
	
}
  if(isset($_POST['product_name']) || isset($_POST['product_description'])){


    $name = sanitize_text_field( $_POST['product_name']);
    $description = sanitize_text_field( $_POST['product_description']);
    // $dob = sanitize_text_field( $_POST['date_of_birth']);
    // $gender = sanitize_text_field( $_POST['patient_gender']);
    // $alergy = sanitize_text_field( $_POST['patient_alergy']);
    $principles = sanitize_text_field( $_POST['principle_duties']);
    

    $wpdb->insert('product',array('name' => $name,'description' => $description,'principles' => $principles));
        }
       