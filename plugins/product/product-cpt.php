<?php
//register patient post type
function dwwp_register_post_type1(){
	$singular = 'Product';
	$plural = 'Products';

	$labels = array(
		'name'               => $plural,
		'singular_name'      => $singular,
		'add_name'           => 'Add New '.$singular,
		'add_new_item'       => 'Add New ' . $singular,
		'edit'               => 'Edit',
		'edit_item'          => 'Edit ' . $singular,
		'new_item'           => 'New ' . $singular,
		'view'               => 'View ' . $singular,
		'search_term'        => 'Search ' . $singular,
		'parent'             => 'Parent ' . $singular,
		'not_found'          => 'No ' . $plural .' found',
		'not_found_in_trash' => 'No ' . $plural .' In Trash'         
		);

	$args = array(
		'labels'              =>  $labels,
		'public'              =>  true,
		'publicly_queryable'  =>  true,
		'exclude_from_search' =>  false,
		'show_in_nav_menus'   =>  true,
		'show_ui'             =>  true,
		'show_in_menu'        =>  true,
		'show_in_admin_bar'   =>  true,
		'menu_position'       =>  5,
		'menu_icon'           =>  'dashicons-businessman',
		'can_export'          =>  true,
		'delete_with_user'    =>  false,
		'hierarchical'        =>  false,
		'has_archive'         =>  true,
		'query_var'           =>  true,
		'capability_type'     =>  'page',
		'map_meta_cap'        =>  true,
		//'capabilities' => array(),
		'rewrite'             => array(
			'slug'       => 'products',
			'with_front' => true,
			'pages'      => true,
			'feeds'      => true,
         	),  
		'supports' => array(
			'title',
			// 'editor',
			// 'author',
			// 'custom-fields',
			// 'excerpt',
			'thumbnail'

			)



		);

register_post_type( 'product' , $args);

}
add_action( 'init', 'dwwp_register_post_type1');