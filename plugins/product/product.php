<?php
/*
Plugin Name: Products
*/

//Exit if accessed directly
if( ! defined( 'ABSPATH' )){

	exit;
}

require_once ( plugin_dir_path(__FILE__) . 'product-cpt.php ');
// require_once ( plugin_dir_path(__FILE__) . 'patients-listing.php ');
require_once ( plugin_dir_path(__FILE__) . 'product-fields.php ');
// require_once ( plugin_dir_path(__FILE__) . 'hospital-shortcode.php ');


//Krijimi i nenmenus List Hospital
function patient_add_submenu_page(){
	add_submenu_page( 
		  'edit.php?post_type=patient',
		  'Reorder Patients',
		  'Reorder Patients', 
		  'manage_options', 
		  'reorder_patients', 
		  'list_admin_patient' );
}
add_action('admin_menu','patient_add_submenu_page');


//Query dhe shfaqja e Produkteve
function list_admin_product(){
		$args  = array(
		'post_type' => 'product',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'post_status' => 'publish',
		'no_found_rows' => true,
		'update_post_term_cache' => false,
		'post_per_page' => 50);
	//'category_name' => 'News');

	$patients_listing = new WP_Query($args);
	
	?>

	<div id="product-sort">
		<div id="icon-patient-admin" class="icon32">
		</div>

		<h2><?php _e('Sort Patients','patients_listing') ?><img src="<?php echo esc_url(admin_url().'/images/loading.gif') ?>" id="loading-animation"></h2>
		<?php if($patients_listing->have_posts()):?>
			<p><?php _e('<strong>Note</strong> this is only the list of Patients','patients_listing'); ?></p>
			<ul id="custom-type-list">
				<?php while($patients_listing->have_posts()): $patients_listing->the_post(); ?>
				<li id="<?php esc_attr(the_id()); ?>"><?php esc_html(the_title()); ?></li>
				<?php endwhile; ?>
			</ul>

		<?php else: ?>
		<p><?php _e('You have no Patients.','patients_listing'); ?></p>
	<?php endif; ?>
	</div>

	<?php
	
}

//Ruajtja e radhitjes se spitaleve
function save_patient_order(){	
	if(!check_ajax_referer('patient-order','security')){
		return wp_send_json_error('Invalid !');
	}
	if(!current_user_can('manage_options')){
		return wp_send_json_error('You are not allowed to do this!');
	}

	$order = $_POST['order'];
	$counter = 0;

	foreach ($order as $item_id) {
		$post = array(
			'ID' => (int)$item_id,
			'menu_order' => $counter
		);
		wp_update_post($post);
		$counter++;
	}
	wp_send_json_success('Reorder Saved');

}
add_action('wp_ajax_save_sort','save_patient_order');



