<?php

// function dwwp_add_custom_metabox() {

// 	add_meta_box(
// 		'dwwp_meta',
// 		__( 'Job Listing' ),
// 		'dwwp_meta_callback',
// 		'job',
// 		'normal',
// 		'core'
// 	);

// }

// add_action( 'add_meta_boxes', 'dwwp_add_custom_metabox' );

function dwwp_meta_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'dwwp_jobs_nonce' );
	$dwwp_stored_meta = get_post_meta( $post->ID ); ?>

	<div>
		

		<!-- <div class="meta-row">
			<div class="meta-th">
				<label for="shop-name" class="dwwp-row-title"><?php _e( 'Shop Name', 'wp-job-listing' ); ?></label>
			</div>
			<div class="meta-td">
				<input type="text" class="dwwp-row-content" name="shop_name" id="shop-name"
				value="<?php if ( ! empty ( $dwwp_stored_meta['shop_name'] ) ) {
					echo esc_attr( $dwwp_stored_meta['shop_name'][0] );
				} ?>"/>
			</div>
		</div>

		<div class="meta-row">
			<div class="meta-th">
				<label for="shop-address" class="dwwp-row-title"><?php _e( 'Shop Address', 'wp-job-listing' ); ?></label>
			</div>
			<div class="meta-td">
				<input type="text" class="dwwp-row-content" name="shop_address" id="shop-address"
				value="<?php if ( ! empty ( $dwwp_stored_meta['shop_address'] ) ) {
					echo esc_attr( $dwwp_stored_meta['shop_address'][0] );
				} ?>"/>
			</div>
		</div>

		<div class="meta-row">
			<div class="meta-th">
				<label for="shop-tel" class="dwwp-row-title"><?php _e( 'Shop Tel', 'wp-job-listing' ); ?></label>
			</div>
			<div class="meta-td">
				<input type="text" class="dwwp-row-content" name="shop_tel" id="shop-tel"
				value="<?php if ( ! empty ( $dwwp_stored_meta['shop_tel'] ) ) {
					echo esc_attr( $dwwp_stored_meta['shop_tel'][0] );
				} ?>"/>
			</div>
		</div>


		<div class="meta">
			<div class="meta-th">
				<span><?php _e( 'Principle Duties', 'wp-job-listing' ) ?></span>
			</div>
		</div>
		<div class="meta-editor"></div>
		<?php

		$content = get_post_meta( $post->ID, 'principle_duties', true );
		$editor = 'principle_duties';
		$settings = array(
			'textarea_rows' => 8,
			'media_buttons' => false,
		);

		wp_editor( $content, $editor, $settings); ?>
</div> -->
		
	<?php
}

// function dwwp_meta_save( $post_id ) {
// 	// Checks save status
//     $is_autosave = wp_is_post_autosave( $post_id );
//     $is_revision = wp_is_post_revision( $post_id );
//     $is_valid_nonce = ( isset( $_POST[ 'dwwp_jobs_nonce' ] ) && wp_verify_nonce( $_POST[ 'dwwp_jobs_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

//     // Exits script depending on save status
//     if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
//         return;
//     }


// 			if ( isset( $_POST[ 'shop_name' ] ) ) {
// 		    	update_post_meta( $post_id, 'shop_name', sanitize_text_field( $_POST['shop_name' ] ) );
// 		    }
// 		    if ( isset( $_POST[ 'shop_address' ] ) ) {
// 		    	update_post_meta( $post_id, 'shop_address', sanitize_text_field( $_POST['shop_address' ] ) );
// 		    }
// 		    if ( isset( $_POST[ 'shop_tel' ] ) ) {
// 		    	update_post_meta( $post_id, 'shop_tel', sanitize_text_field( $_POST['shop_tel' ] ) );
// 		    }
// 		    if ( isset( $_POST[ 'principle_duties' ] ) ) {
// 		    	update_post_meta( $post_id, 'principle_duties', sanitize_text_field($_POST[ 'principle_duties' ] ) );
// 		    }



// }
// add_action( 'save_post', 'dwwp_meta_save' );







