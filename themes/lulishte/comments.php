<?php if(have_comments()):?>
	<h4 id="comments"><?php comments_number('No Comments','Comments(1)','Comments(%)'); ?></h4>
	<div class="comment-list">
		<?php wp_list_comments('callback=the_comments'); ?>
		<div class="navigation">
		<?php paginate_comments_links(); ?> 
		 </div>
	</div>
<?php endif; ?>
<?php
	$args = array(
		'label_submit' => 'Submit',
		'title_reply' => 'Post Comment',
		'comment_notes_after' => ''
	);
	comment_form($args);

?>