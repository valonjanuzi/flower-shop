<?php get_header(); ?>

		
    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('images/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="<?php echo get_template_directory_uri(); ?>/images/1-slider.jpg" />
                <div style="position: absolute; top: 100px; left: 80px; width: 480px;font-weight:bold; height: 180px; font-size: 40px;    font-family: 'Crimson Text'; color: #e60000; line-height: 80px;">A PERFECT BOUQUET</div>
                <div style="position: absolute; top: 200px; left: 80px; width: 480px; height: 120px; font-size: 15px; font-family: Arial;color: #0d0d0d; line-height: 27px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  </div>
     
                <div data-u="caption" data-t="5" style="position: absolute; top: 120px; left: 650px; width: 470px; height: 220px;">
         
                    <div style="position: absolute; top: 4px; left: 45px; width: 379px; height: 213px; overflow: hidden;">
             
                    </div>
                 
                </div>
            </div>
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="<?php echo get_template_directory_uri(); ?>/images/2-slider.jpg" />
                <div style="position: absolute; top: 100px; left: 80px; width: 480px;font-weight:bold;  height: 180px; font-size: 42px;    font-family: 'Crimson Text'; color: #e60000; line-height: 80px;">LOCALLY GROWN</div>
                <div style="position: absolute; top: 200px; left: 80px; width: 480px; height: 120px; font-size: 15px; font-family: Arial;color: #0d0d0d; line-height: 27px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.   </div>
            </div>
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="<?php echo get_template_directory_uri(); ?>/images/3-slider.jpg" />
                <div style="position: absolute; top: 100px; left: 80px; font-weight:bold; width: 480px; height: 180px; font-size: 42px;    font-family: 'Crimson Text'; color: #e60000; line-height: 80px;">BEST FLOWER</div>
                <div style="position: absolute; top: 200px; left: 80px; width: 480px; height: 120px; font-size: 15px; font-family: Arial;color: #0d0d0d; line-height: 27px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </div>
            </div>
            <a data-u="ad" href="http://www.jssor.com" style="display:none">jQuery Slider</a>
        
        </div>
 
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>

    <!-- #endregion Jssor Slider End -->

<div class="container">
<div class="row parts">
	<?php

 if ( have_posts() ) : ?>
	<?php while (have_posts()) : the_post(); ?>
		


<div class="col-xs-12 col-sm-6 col-lg-4">
		<div class="image">  <?php the_post_thumbnail('large'); ?> </div>
<div class="content-wrap"><h3><?php the_title();?></h3>
<?php if ($post->post_excerpt): ?>
					<p class="not-content">
						<?php echo get_the_excerpt(); ?>
					</p>

					<a class="btn btn-sm slider" href="<?php the_permalink();?>">Read more</a>
				<?php else: ?>
						<?php the_content(); ?>
						<a class="btn btn-sm slider" href="<?php the_permalink();?>">Read more</a>
				<?php endif; ?>
				</div>

</div>


	<?php endwhile; ?>
	<?php else : ?>

	<?php endif; ?>
	</div>
</div>




<div class="ourprodukti">
		
		<div class="container">
			<h2>Our Bouqest</h2>
			<div class="item">
	            <ul id="content-slider" class="content-slider">
		                <li>	                	
							
			              <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/foto-product1.jpg" alt="Smiley face" height="" width=""></a>
			               			                 
		                </li>
		                <li>
		                <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/foto-product2.jpg" alt="Smiley face" height="" width=""></a>
		                    
		                </li>
		                <li>
		                 <a href="#">  	<img src="<?php echo get_template_directory_uri();?>/images/foto-product3.jpg" alt="Smiley face" height="" width=""></a>
		                    
		                </li>
		                <li>
		                 <a href="#">  	<img src="<?php echo get_template_directory_uri();?>/images/foto-product4.jpg" alt="Smiley face" height="" width=""></a>
		                    
		                </li>
		                <li>
		                  <a href="#"> 	<img src="<?php echo get_template_directory_uri();?>/images/foto-product5.jpg" alt="Smiley face" height="" width=""></a>
		                    
		                </li>
		                <li>
		                  <a href="#"> 	<img src="<?php echo get_template_directory_uri();?>/images/foto-product6.jpg" alt="Smiley face" height="" width=""></a>
		                   
		                </li>
	              
	            </ul>
       		</div>
		</div>

	</div>

<div class="container">
<div class="row parts2">
	<div class="afoto col-sm-6">
	<img class="image" src="<?php echo get_template_directory_uri(); ?>/images/florist-pointing.jpg"/>
		
	</div>
	<div class="apart col-sm-6">
		<div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h1><span style="color: #cd0236;">ARRANGE YOUR OWN BOUQUET</span></h1>

		</div>
	</div>
<div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut cursus euismod odio at suscipit. Aliquam erat volutpat. Suspendisse tempus arcu at augue mollis dapibus. Cras sollicitudin metus ligula, ut consequat orci pellentesque nec. Phasellus quam erat, aliquet et nibh a, aliquam elementum justo.</p>

		</div>
		<button class="btn btn-md style-1" id="custom-id" style="">TYPE FOR REQUEST</button>
	</div>

	</div>
</div>
</div>
</div>

	<div class="boyflower">
		
		<div class"container ">
			<h2>PERFECT FLOWERS FOR EVERY OCCASION </h2>
		</div>

	</div>


<div class="container">
	<div id="over-footer" calss="row">
	
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<figure class="img-container">
				<svg class="recentblog-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#blog-icon"></use></svg><time class="recentblog-time" datetime="2014-12-12 12:37">12<span><br>Dec</span></time>
				<a href="#"><div class="opacity-div"><i class="fa fa-link fa-3x" aria-hidden="true"></i></div></a>
				<img id="img1" src="<?php echo get_template_directory_uri();?>/images/doityourself.jpg">
			</figure>
			<a href="#"><h2>Lorem ipsum</h2></a>
			<p>
				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English
			</p>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<figure class="img-container">
			<svg class="recentblog-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#blog-icon"></use></svg><time class="recentblog-time" datetime="2014-12-12 12:37">12<br><span>Dec</span></time>
				<a href="#"><div class="opacity-div"><i class="fa fa-link fa-3x" aria-hidden="true"></i></div></a>
				<img id="img2" src="<?php echo get_template_directory_uri();?>/images/tipsandtricks.jpg">
			</figure>
			<a href="#"><h2>Lorem ipsum</h2></a>
			<p>
				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English
			</p>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<figure class="img-container">
			<svg class="recentblog-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#blog-icon"></use></svg><time class="recentblog-time" datetime="2014-12-12 12:37">12<br><span>Dec</span></time>
				<a href="#"><div class="opacity-div"><i class="fa fa-link fa-3x" aria-hidden="true"></i></div></a>
				<img id="img3" src="<?php echo get_template_directory_uri();?>/images/wrapping.jpg">
			</figure>
			<a href="#"><h2>Lorem ipsum</h2></a>
			<p>
				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English
			</p>
		</div>
	</div>
</div>
<?php get_footer(); ?>