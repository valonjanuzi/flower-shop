<?php get_header(); ?>

<div class="container about-us">

	<h1>About us</h1>

	<div class="col-sm-6 aboutdiv">
		<img id="" class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/about-us1.jpg">
		<h3>COMPANY HISTORY</h3>
		<p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo</p>

	</div>

	<div class="col-sm-6 aboutdiv">
		<img id="" class="img-responsive"src="<?php echo get_template_directory_uri();?>/images/about-us2.jpg">
		<h3>COMPANY MISSION</h3>
		<p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>

	</div>


	<div class="aboutpjesa">
		<div class="col-sm-4 aboutdiv2 ">
			<div class="hovereffect">
				
				<img id=""class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/about-foto1.jpg">
				<div class="overlay">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent mattis aliquet purus non elementum. Nam quis vulputate enim, congue ullamcorper risus.</p>
				</div>	

			  
	    	</div>	
			
			<h2>Dona Boss</h2>
			<h4>CEO</h4>

		</div>

		<div class="col-sm-4 aboutdiv2">
			<div class="hovereffect">
			<img id=""class="img-responsive"  src="<?php echo get_template_directory_uri();?>/images/about-foto2.jpg">
				<div class="overlay">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent mattis aliquet purus non elementum. Nam quis vulputate enim, congue ullamcorper risus.</p>
				</div>	
			</div>
			<h2>Yenna Bloom</h2>
			<h4>Decorator</h4>

		</div>

		<div class="col-sm-4 aboutdiv2">
			<div class="hovereffect">
			<img id="" class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/about-foto3.jpg">

				<div class="overlay">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent mattis aliquet purus non elementum. Nam quis vulputate enim, congue ullamcorper risus.</p>
				</div>	
			</div>
			<h2>George Green</h2>
			<h4>Botanical expert</h4>

		</div>
	</div>


</div>

<?php get_footer(); ?>