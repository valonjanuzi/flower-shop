		
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-sm-6 col-lg-3">
						<?php if (is_active_sidebar('footer1')) :?>
							<?php dynamic_sidebar('footer1'); ?>
						<?php endif; ?>	
					</div>
					<div class="col-sm-12 col-sm-6 col-lg-3">
						<?php if (is_active_sidebar('footer2')) :?>
							<?php dynamic_sidebar('footer2'); ?>
						<?php endif; ?>
					</div>
					<div class="col-sm-12 col-sm-6 col-lg-3">
						<?php if (is_active_sidebar('footer3')) :?>
							<?php dynamic_sidebar('footer3'); ?>
						<?php endif; ?>
					</div>	
					<div class="col-sm-12 col-sm-6 col-lg-3">
						<?php if (is_active_sidebar('footer4')) :?>
							<?php dynamic_sidebar('footer4'); ?>
						<?php endif; ?>
					</div>		
				</div>
			</div>
			
		</div>
		<div class="under-footer">
			<p><?php bloginfo('name'); ?> - &copy; <?php echo date('Y');?></p>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>