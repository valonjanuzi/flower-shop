
<?php get_header(); ?>

<?php if(have_posts()): ?>
	<div class="single-page-title"><h1><?php the_title(); ?></h1></div>
	<div class="container">
	<div class="single-post">
		<?php while(have_posts()): the_post(); ?>

			<?php if(has_post_thumbnail()): ?>
				<div class="single-post-img"><?php the_post_thumbnail('featured'); ?></div>	
			<?php endif; ?>

			<ul>
				<a href="<?php the_permalink(); ?>"><li id="not-comment"><?php comments_number(); ?></li></a>
				<?php if(has_tag()): ?><li id="not-tag"><a href="<?php echo get_tag_link($tag_id); ?>"><?php the_tags(); ?></a><?php  ?></li><?php endif; ?>
				<li id="not-date"><?php the_time('F j, Y g:i a'); ?></li>
			</ul>

			<h1 class="single-post-title"><?php the_title(); ?></h1>

			<div class="single-content">
				<?php the_content(); ?>

				<?php if (in_category('products')) { ?>
				<button id="show-form" name="button">Order now</button>

				<div id="request-form">
	
					<form method="POST" id="my-form" action="<?php bloginfo('stylesheet_directory'); ?>/insert-request.php">
						<p id="close-form">X</p>
						<label>Product Name:</label><input type="text" name="product" value="<?php the_title(); ?>">
						<label>Name:</label><input type="text" name="name" class="name">
						<label>E-mail:</label><input type="text" name="e-mail" class="e-mail">
						<label>Phone:</label><input type="text" name="phone" class="phone">
						<label>Request Details:</label><textarea class="details" name="details"></textarea>
						<span id="response"></span>
						<input type="submit" name="order" class="order" value="Order">
						
					</form>
					
				</div>
			<?php } ?>
			</div>

			

		<?php endwhile; ?>
		</div>
		<div class="single-right">
			<?php if (is_active_sidebar('not-left-sidebar')) :?>
				<?php dynamic_sidebar('not-left-sidebar'); ?>
			<?php endif; ?>
		</div>
		<div class="my-comments">	
			<?php comments_template(); ?>
		</div>
	</div>
<?php endif; ?>
<?php get_footer(); ?>