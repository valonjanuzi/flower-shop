<?php

/**
 * Proper way to enqueue scripts and styles
 */
function external_resources(){
  if(get_option( 'thread_comments' ) ){
      wp_enqueue_script('comment-reply');
    }
    wp_enqueue_style('style.css' , get_stylesheet_uri());
    wp_enqueue_style( 'bootstrap-min-style', "http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" );
    wp_enqueue_script( 'jquery-min', get_template_directory_uri() . '/js/jquery-1.11.3.min.js', array(), '1.11.3', true );
    wp_enqueue_script( 'script-cycle', get_template_directory_uri() . '/js/cycle2.js',array(),'',true);
    wp_enqueue_script( 'bootstrap.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js',array('jquery'), true );
    wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.js',array(), '1.11.4', true);
    wp_enqueue_script( 'my-script', get_template_directory_uri() . '/js/script.js',array(), '', true);

    wp_enqueue_script( 'lightslider', get_template_directory_uri() . '/js/lightslider.js',array(), '', true);
    // Slideri
      wp_enqueue_script( 'jssor.slider.mini', get_template_directory_uri() . '/js/jssor.slider.mini.js',array(), '', true);
    // Slideri

    
}

add_action('wp_enqueue_scripts' , 'external_resources');

add_theme_support( 'post-thumbnails' ); 
add_theme_support('html5',array('search-form'));


// Add Your Menu Locations
function register_my_menus() {
  register_nav_menus(
    array(  
      'header_menu' => __( 'Header menu' ), 
    )
  );
} 

add_action( 'init', 'register_my_menus' );

function flowerWidgetsInit(){

    register_sidebar(array(
      'name'=>'Footer Area 1',
      'id'=>'footer1',
      'before_widget'=>'<div class="widget-item search1">',
      'after_widget' =>'</div>'
      ));
     register_sidebar(array(
      'name'=>'Footer Area 2',
      'id'=>'footer2',
      'before_widget'=>'<div class="widget-item ">',
      'after_widget' =>'</div>'
      ));
     register_sidebar(array(
      'name'=>'Footer Area 3',
      'id'=>'footer3',
      'before_widget'=>'<div class="widget-item ">',
      'after_widget' =>'</div>'
      ));
     register_sidebar(array(
      'name'=>'Footer Area 4',
      'id'=>'footer4',
      'before_widget'=>'<div class="widget-item ">',
      'after_widget' =>'</div>'
      ));

     register_sidebar(array(
      'name'=>'Left Sidebar',
      'id'=>'left-sidebar',
      'before_widget'=>'<div class="leftsidebar-widget-item ">',
      'after_widget' =>'</div>'
      ));

     register_sidebar(array(
      'name'=>'Notifications Left Sidebar',
      'id'=>'not-left-sidebar',
      'before_widget'=>'<div class="not-leftsidebar-widget-item ">',
      'after_widget' =>'</div>'
      ));
  

}
add_action('widgets_init','flowerWidgetsInit');

function flowershop_setup() {
  add_theme_support('featured');
}
add_action('init','flowershop_setup');


function the_comments($comment,$args,$depth){
    
    $GLOBALS['comment'] = $comment;

  ?>
    <li <?php comment_class(); ?>>
      <div id="comment-<?php comment_ID(); ?>">
        <div class="comment-header">
          <?php printf(__('<cite class="author">%s</cite>','blue'), get_comment_author_link()); ?>
          <div class="comment-meta commentmetadata">
            On <?php comment_date(); ?> at <?php comment_time(); ?>
            <?php edit_comment_link(__('Edit','blue'),' ',''); ?>
            <button class="reply-link " id="reply-button">
              <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
            </button>
          </div>
        </div>

        <?php if($comment->comment_approved == '0'): ?>
          <div class="waiting-approvement">Your comment is waiting approvement.</div>
        <?php endif; ?>

        <?php comment_text(); ?>

        
      </div>
    </li>

  <?php
  }

  function flower_register_post_type(){
  $singular = 'User Search';
  $plural = 'User Searches';

  $labels = array(
    'name'               => $plural,
    'singular_name'      => $singular,
    // 'add_name'           => 'Add New '.$singular,
    // 'add_new_item'       => 'Add New ' . $singular,
    // 'edit'               => 'Edit',
    // 'edit_item'          => 'Edit ' . $singular,
    // 'new_item'           => 'New ' . $singular,
    // 'view'               => 'View ' . $singular,
    // 'search_term'        => 'Search ' . $singular,
    // 'parent'             => 'Parent ' . $singular,
    // 'not_found'          => 'No ' . $plural .' found',
    // 'not_found_in_trash' => 'No ' . $plural .' In Trash'         
    );

  $args = array(
    'labels'              =>  $labels,
    'public'              =>  true,
    'publicly_queryable'  =>  true,
    'exclude_from_search' =>  false,
    'show_in_nav_menus'   =>  true,
    'show_ui'             =>  true,
    'show_in_menu'        =>  true,
    'show_in_admin_bar'   =>  true,
    'menu_position'       =>  10,
    'menu_icon'           =>  'dashicons-businessman',
    'can_export'          =>  true,
    'delete_with_user'    =>  false,
    'hierarchical'        =>  false,
    'has_archive'         =>  true,
    'query_var'           =>  true,
    'capability_type'     =>  'page',
    'map_meta_cap'        =>  true,
    //'capabilities' => array(),
    'rewrite'             => array(
      'slug'       => 'user-search',
      'with_front' => true,
      'pages'      => true,
      'feeds'      => true,
          ),  
    'supports' => array(
      'title'
      // 'editor',
      // 'author',
      // 'custom-fields',
      // 'excerpt',
      // 'thumbnail'

      )



    );

register_post_type( 'search' , $args);

}
add_action( 'init', 'flower_register_post_type');

function flower_add_submenu_page(){
  add_submenu_page( 
      'edit.php?post_type=search',
      'Last 500 Searches',
      'Last 500 Searches', 
      'manage_options', 
      'list_search', 
      'list_flower_search' );

}
add_action('admin_menu','flower_add_submenu_page');

function list_flower_search(){
   global $wpdb;
  $res = $wpdb->get_results("SELECT * FROM search ORDER BY id ASC LIMIT 500");
  if($res){
    echo '<div class="search-list">';
    echo '<h3>Last 500 searches</h3>';
    echo '<table cellspacing="0">';
    echo '<tr>';
    echo '<td style="font-weight: bold;border: 1px solid #857C7C;
    padding: 5px;
    margin: 0;">Name</td>';
    echo '<td style="font-weight: bold;border: 1px solid #857C7C;
    padding: 5px;
    margin: 0;">Date</td>';
    echo '</tr>';
    foreach ($res as $result) {
      echo '<tr>';
      echo '<td style="border: 1px solid #857C7C;
    padding: 5px;
    margin: 0;">'.$result->title.'</td>';
      echo '<td style="border: 1px solid #857C7C;
    padding: 5px;
    margin: 0;">'.$result->date.'</td>';
      echo '</tr>';
    }
    echo '</table>';
    echo '</div>';
  }
}


