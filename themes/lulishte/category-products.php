<?php
	get_header();
?>
<div class="page-heading style-2">
                    <div class="container">
                                <h1>Projects</h1>                    </div>
                </div>
   

	<div id="over-footer" calss="row">
	<ul class="filter style-1"><li><i style="color: #000000;" class="fa fa-filter"></i></li><li><button style="color: #000000;" data-filter="*" class="selected">All</button></li><li><span style="color: #000000;">/</span></li><li><button style="color: #000000;" data-filter="flowers">Flowers</button></li><li><span style="color: #000000;">/</span></li><li><button style="color: #000000;" data-filter="imported-flowers" class="">Imported flowers</button></li><li><span style="color: #000000;">/</span></li><li><button style="color: #000000;" data-filter="local-flowers">Local flowers</button></li><li><span style="color: #000000;">/</span></li><li><button style="color: #000000;" data-filter="fresh-flowers">Fresh flowers</button></li></ul>
	<?php

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		?>
	
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<figure class="img-container">

				<a href="<?php the_permalink(); ?>"><div class="opacity-div"><span class="lule"><?php the_title(); ?></span></div></a>

				 <?php the_post_thumbnail('large'); ?> 
				 
			</figure>
		</div>
<!-- 			<button id="show-form" name="button">Order now</button>
 -->		
		
		<?php

	} // end while
} // end if
?>

	</div>
	<!-- <div id="request-form">
	
		<form method="POST" id="my-form" action="<?php bloginfo('stylesheet_directory'); ?>/insert-request.php">
			<p id="close-form">X</p>
			<label>Product URL:</label><input type="text" name="url" value="<?php the_permalink(); ?>">
			<label>Name:</label><input type="text" name="name" class="name">
			<label>E-mail:</label><input type="text" name="e-mail" class="e-mail">
			<label>Phone:</label><input type="text" name="phone" class="phone">
			<label>Request Details:</label><textarea class="details" name="details"></textarea>
			<span id="response"></span>
			<input type="submit" name="order" class="order" value="Order">
			
		</form>
		
	</div> -->
  </div>
                                    
<?php get_footer(); ?>
  

