<?php get_header(); ?>

<div class="services-name">
	<div class="container">
		<h2 class="page-name">Services</h2>
	</div>
</div>
<div class="container">
	<div class="left-sidebar">
		<ul class="all-services">
			<li>All Services</li>
		</ul>
		<?php if (is_active_sidebar('left-sidebar')) :?>
			<?php dynamic_sidebar('left-sidebar'); ?>
		<?php endif; ?>
	</div>

	<div class="right-container">
		<?php if ( have_posts() ) {
			while ( have_posts() ) {
				the_post(); 
				?>
			<div>
				<?php the_post_thumbnail('large'); ?>
				<h3><?php the_title(); ?></h3>
				<?php if ($post->post_excerpt): ?>
					<p class="not-content">
						<?php echo get_the_excerpt(); ?>
					</p>

					<a class="services-read" href="<?php the_permalink(); ?>">Read More</a>
				<?php else: ?>
						<?php the_content(); ?>
				<?php endif; ?>
				
			</div>
			<?php } ?>
		<?php } ?>
			
	</div>
</div>
<?php get_footer(); ?>