<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
			<title></title>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0">
				 <?php if ( is_singular() && get_option( 'thread_comments' ) && comments_open() ) wp_enqueue_script( 'comment-reply' );
				 wp_head();
				 ?>
			<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/lightslider.css" type="text/css" media="screen" />
			<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/ihover.css" type="text/css" media="screen" />
			<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/ihover.min.css" type="text/css" media="screen" />
	</head>
<body>

<header>

<div class="top-bar">
       <!--  <div class="container" data-height="40" style="height: 0;"><ul class="left"><li id="anpssocial-1" class="widget-container widget_anpssocial">  -->               <ul class="social">
                        <li>
                                <a class="fa fa-facebook" href="#" target="_self"></a>
                            </li>
                                    <li>
                                <a class="fa fa-twitter" href="#" target="_self"></a>
                            </li>
                                    <li>
                                <a class="fa fa-linkedin" href="#" target="_self"></a>
                            </li>
                                    <li>
                                <a class="fa fa-google-plus" href="#" target="_self"></a>
                            </li>
                                                                                                                                </ul>
        </li></ul><ul class="right"><li id="anpstext-1" class="widget-container widget_anpstext">
        <span class="fa fa-clock-o"></span>
        Mon - Sat: 7:00 - 17:00        </li><li id="anpstext-2" class="widget-container widget_anpstext">
        <span class="fa fa-phone"></span>
        + 386 40 111 5555        </li><li id="anpstext-3" class="widget-container widget_anpstext">
        <span class="fa fa-envelope-o"></span>
        <a href="mailto:info@yourdomain.com">info@yourdomain.com</a>        </li></ul><div class="clearfix"></div>    <span class="close fa fa-chevron-down"></span>
        </div>
	<nav class="navbar navbar-default navbar1">
			  	<div class="container">
	  		<a href="home"><img id="logo" width="160" height="90" src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a> 
			  		<div class="navbar-header">
				  			<button type="button" class="navbar-toggle " data-toggle="collapse" data-target="#collapse" aria-expanded="false">

						        <span class="sr-only">Toggle navigation</span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>

					     	</button>
				    	<a class="navbar-brand" href="#featured"></a>
					</div>
					<div  class="collapse navbar-collapse" id="collapse">

					<?php 
							$main = array(
								'theme_location'  => 'header_menu',
								'container'       => 'div',
								'container_class' => 'navbar-collapse',
								'container_id'    => 'bs-example-navbar-collapse-1',
								'menu_class'      => '',
								'menu_id'         => '',
								'echo'            => true,
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul class="nav navbar-nav navbar-right">%3$s</ul>',
								'depth'           => 0,
								'walker'          => ''
							);


					        wp_nav_menu($main);
					        ?>
					        <div class="search">
					       <?php get_search_form();?>
					        </div>

					</div>
					

				</div> 
			        
			        
				
			</nav>
</header>