<?php get_header();  ?>
<div class="single-page-title"><h1>Notifications</h1></div>
<div class="container">
<?php	if(have_posts()): ?>

	
		
		<?php while(have_posts()): the_post(); ?>

			<div class="notifications-left">
				<?php if(has_post_thumbnail()): ?>
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('featured'); ?></a>	
				<?php endif; ?>
				
				<ul>
					<a href="<?php the_permalink(); ?>"><li id="not-comment"><?php comments_number(); ?></li></a>
					<?php if(has_tag()): ?><li id="not-tag"><a href="<?php echo get_tag_link($tag_id); ?>"><?php the_tags(); ?></a><?php  ?></li><?php endif; ?>
					<a href"#"><li id="not-date"><?php the_time('F j, Y g:i a'); ?></li></a>
				</ul>
				<a class="a-not-title" href="<?php the_permalink(); ?>"><h2 class="not-title"><?php the_title(); ?></h2></a>

				<?php if ($post->post_excerpt): ?>
					<p class="not-content">
						<?php echo get_the_excerpt(); ?>
					</p>

					<a class="not-readmore" href="<?php the_permalink(); ?>">Read More</a>
				<?php else: ?>
						<?php the_content(); ?>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>			
		

<?php else: ?>
			<p>There is no post to show</p>
<?php endif; ?>
<div class="notifications-right">
	<?php if (is_active_sidebar('not-left-sidebar')) :?>
		<?php dynamic_sidebar('not-left-sidebar'); ?>
	<?php endif; ?>
</div>
<div class="next-prev">
			<?php posts_nav_link( ' &#183; ', '<span class="prev-button">Previous page</span>', '<span class="next-button">Next page</span>' ); ?>
		</div>
</div>

<?php get_footer(); ?>