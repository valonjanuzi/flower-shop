<?php

 get_header();

?>
	<div class="container">
		<div class="row">
		<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		?>
	
		<?php get_template_part('content', 'search'); ?>

		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		<?PHP
		the_content();
	} // end while?>

	</div>
		</div>

<?php } // end if
	

get_footer();